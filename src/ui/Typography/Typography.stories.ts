import type { Meta, StoryObj } from '@storybook/vue3'

import Typography from './Typography.vue'

const meta = {
  title: 'UI/Typography',
  tags: ['autodocs'],
  component: Typography,
  argTypes: {
    class: {
      control: 'select',
      options: [
        'display',
        'heading',
        'label',
        'paragraph',
        'mono_display',
        'mono_heading',
        'mono_label',
        'mono_paragraph'
      ]
    },
    size: {
      control: 'select',
      options: ['xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge']
    }
  },
  args: {}
} satisfies Meta<typeof Typography>

export default meta

type Story = StoryObj<typeof meta>
export const Default: Story = {
  args: {}
}
